/**
 * GET /
 * Home page
 */
exports.index = (req, res) => {
  res.render('index', {
    title: 'Home',
    bodyText: 'Welcome to NodeJs Fest'
  });
};