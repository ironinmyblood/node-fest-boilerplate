
const express = require("express");
const logger = require('morgan');
const bodyParser = require('body-parser');
const compression = require('compression');
const path = require('path');
const sass = require('node-sass-middleware');
const dotenv = require('dotenv');
let app =  express();

// CONTROLLER
const homeController = require('./controllers/home');

// VIEWS
app.set('view engine', 'ejs');
app.set('views', __dirname + '/views');
// MIDDLEWARES
app.use(sass({
    src: path.join(__dirname, 'public'),
    dest: path.join(__dirname, 'public')
}));
app.use(logger('dev'));
app.use(compression());
dotenv.load({ path: '.env.config' });
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use('/', express.static(path.join(__dirname, 'public'), { maxAge: 31557600000 }));
app.use('/js/lib', express.static(path.join(__dirname, 'node_modules/popper.js/dist/umd'), { maxAge: 31557600000 }));
app.use('/js/lib', express.static(path.join(__dirname, 'node_modules/bootstrap/dist/js'), { maxAge: 31557600000 }));
app.use('/js/lib', express.static(path.join(__dirname, 'node_modules/jquery/dist'), { maxAge: 31557600000 }));
app.use('/webfonts', express.static(path.join(__dirname, 'node_modules/@fortawesome/fontawesome-free/webfonts'), { maxAge: 31557600000 }));

/**
 *  PRIMARY APP ROUTES GOES HERE 
 */
app.get('/', homeController.index);

app.listen(process.env.PORT, () => {
    console.log(`Sever running at ${process.env.PORT}`);
})